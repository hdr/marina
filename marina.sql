drop database if exists marina;
create database marina;

\c marina

create table bote(
	bid    int,
	color  varchar(64),
	nombre varchar(64)
);

create table marine(
	mid           int,
	nombre        varchar(64),
	puntaje       int,
	f_inscripción date
);

create table alquiler(
	bid   integer,
	mid   integer,
	fecha date
);

create table reserva(
	bid   integer,
	mid   integer,
	fecha date
);

alter table marine   add constraint marine_pk    primary key (mid);
alter table bote     add constraint bote_pk      primary key (bid);
alter table reserva  add constraint reserva_pk   primary key (bid, mid, fecha);
alter table alquiler add constraint alquiler_pk primary key (bid, mid, fecha);

alter table alquiler add constraint alquiler_mid_fk foreign key (mid) references marine(mid);
alter table alquiler add constraint alquiler_bid_fk foreign key (bid) references bote(bid);

alter table reserva add constraint reserva_mid_fk foreign key (mid) references marine(mid);
alter table reserva add constraint reserva_bid_fk foreign key (bid) references bote(bid);

insert into marine values(1, 'Lucifer', 100, '1999-01-01');
insert into marine values(2, 'Satan', 99, '2000-01-01');
insert into marine values(3, 'Belzebu', 98, '2001-01-01');
insert into marine values(4, 'Samael', 97, '2002-01-01');
insert into marine values(5, 'Azrael', 70, '2020-03-02');

insert into bote values(1, 'blue', 'Macross');
insert into bote values(2, 'red', 'SDF-1');
insert into bote values(3, 'yellow', 'Megadrive');
insert into bote values(4, 'black', 'Perla Negra');
insert into bote values(5, 'black', 'Arcadia');

insert into reserva values(1, 2, '2013-02-01');
insert into reserva values(2, 3, '2013-03-02');
insert into reserva values(2, 3, '2011-06-26');
insert into reserva values(3, 4, '2013-05-04');
insert into reserva values(3, 1, '2013-02-06');
insert into reserva values(2, 4, '2013-07-01');
insert into reserva values(1, 2, '2011-06-26');

insert into alquiler values(1, 2, '2011-02-01');
insert into alquiler values(2, 2, '2011-02-01');
insert into alquiler values(3, 2, '2011-02-01');
insert into alquiler values(4, 2, '2011-02-01');
insert into alquiler values(5, 2, '2011-02-01');
insert into alquiler values(2, 3, '2011-03-02');
insert into alquiler values(3, 4, '2012-05-04');
insert into alquiler values(3, 1, '2012-02-06');
insert into alquiler values(2, 4, '2013-07-01');
insert into alquiler values(1, 4, '2013-07-01');
insert into alquiler values(3, 4, '2011-06-26');
insert into alquiler values(5, 4, '2011-06-26');
insert into alquiler values(2, 1, '2011-06-26');
insert into alquiler values(4, 4, '2011-06-26');
